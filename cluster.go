package scylla

import (
	"log"
	"os"

	"github.com/gocql/gocql"
)

func createCluster() *gocql.ClusterConfig {
	cluster := gocql.NewCluster(os.Getenv("CQLSH_HOST"))
	cluster.Keyspace = os.Getenv("SCYLLA_KEYSPACE")
	cluster.Consistency = gocql.Quorum
	cluster.ProtoVersion = 4
	cluster.PoolConfig.HostSelectionPolicy = gocql.TokenAwareHostPolicy(gocql.RoundRobinHostPolicy())

	return cluster
}

func CreateSession() *gocql.Session {
	cluster := createCluster()

	session, err := cluster.CreateSession()
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("succefully connected to scylla cluster @ %s", os.Getenv("CQLSH_HOST"))
	return session
}

package scylla

import "time"

type Device struct {
	Id string `validate:"required,uuid4" db:"device_id"`
}

type LocationModel struct {
	Time time.Time `validate:"required,lte" db:"time"`
	Lat  float32   `validate:"gte=-180,lte=180" db:"lat"`
	Long float32   `validate:"gte=-180,lte=180" db:"long"`
}

type DeviceTimeRange struct {
	UUID  string    `validate:"required,uuid4" db:"device_id"`
	Start time.Time `validate:"required" db:"start"`
	End   time.Time `validate:"required,lte,gtefield=Start" db:"end"`
}

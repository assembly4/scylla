package scylla

import (
	"context"
	"log"

	"github.com/gocql/gocql"
	"github.com/scylladb/gocqlx"
	"github.com/scylladb/gocqlx/qb"
	"github.com/scylladb/gocqlx/table"
)

var stmts = createStatements()

type query struct {
	stmt  string
	names []string
}

type statements struct {
	ins query
	sel query
}

func createStatements() *statements {
	m := table.Metadata{
		Name:    "device_location_v1",
		Columns: []string{"device_id", "time", "lat", "long"},
		PartKey: []string{"device_id", "time"},
	}

	tbl := table.New(m)
	insertStmt, insertDeviceLoc := tbl.Insert()
	selectStmt, selectTimeFrame := qb.Select(m.Name).
		Columns("time", "lat", "long").
		Where(
			qb.Eq("device_id"),
			qb.GtOrEqNamed("time", "start"),
			qb.LtOrEqNamed("time", "end")).
		ToCql()

	return &statements{
		ins: query{
			stmt:  insertStmt,
			names: insertDeviceLoc,
		},
		sel: query{
			stmt:  selectStmt,
			names: selectTimeFrame,
		},
	}
}

func SelectQuery(session *gocql.Session, queryModel DeviceTimeRange, ch chan []LocationModel) error {
	var (
		rs       []LocationModel
		nextPage []byte
		err      error
	)

	for {
		rs, nextPage, err = getDeviceLoc(session, queryModel, nextPage)
		if err != nil {
			log.Fatalln(err)
			return err
		}
		if len(rs) == 0 {
			close(ch)
			break
		}
		ch <- rs
		if len(nextPage) == 0 {
			close(ch)
			break
		}
	}

	log.Printf("select for device %s successful", queryModel.UUID)
	return nil
}

func getDeviceLoc(session *gocql.Session, qM DeviceTimeRange, page []byte) (locs []LocationModel, nextPage []byte, err error) {
	q := gocqlx.Query(session.Query(stmts.sel.stmt), stmts.sel.names).BindStruct(qM)
	defer q.Release()
	q.PageState(page)
	q.PageSize(500)

	iter := q.Iter()
	return locs, iter.PageState(), iter.Select(&locs)
}

func InsertBatchQuery(session *gocql.Session, device Device, data []LocationModel) error {
	ctx := context.Background()
	batch := session.NewBatch(gocql.LoggedBatch).WithContext(ctx)

	for _, object := range data {
		batch.Query(stmts.ins.stmt, device.Id, object.Time, object.Lat, object.Long)
	}

	if err := session.ExecuteBatch(batch); err != nil {
		log.Fatalln(err)
		return err
	}

	log.Printf("batch insert for device %s successful", device.Id)
	return nil
}
